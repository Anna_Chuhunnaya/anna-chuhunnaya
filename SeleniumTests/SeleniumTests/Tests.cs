﻿using System;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;


namespace SeleniumTests
{
    [TestFixture]
    public class Tests
    {
        IWebDriver driver = new ChromeDriver(@"D:\ChromeDriver");
        [Test]
        public void Searchflights()
        {           
            driver.Navigate().GoToUrl("https://wizzair.com");
            WebDriverWait waitLong = new WebDriverWait(driver, TimeSpan.FromSeconds(80));
            WebDriverWait waitShort = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            IWebElement DepartureStation = waitLong.Until(o=> o.FindElement(By.Id("search-departure-station")));
            DepartureStation.Clear();
            DepartureStation.SendKeys("Kiev");
            IWebElement DepartureStationKiev = driver.FindElement(By.XPath("//strong[@class='locations-container__location__name']"));
            DepartureStationKiev.Click();

            IWebElement ArrivalStation = driver.FindElement(By.Id("search-arrival-station"));
            ArrivalStation.SendKeys("Copenhagen");
            IWebElement ArrivalStationCopenhagen = driver.FindElement(By.XPath("//strong[@class='locations-container__location__name']"));
            ArrivalStationCopenhagen.Click();

            IWebElement SelectedDateElement = driver.FindElement(By.Id("search-departure-date"));
            string SelectedDate = SelectedDateElement.Text;
            string SelectedDate1 = SelectedDate.Substring(0, SelectedDate.Length - 5);
            string SelectedDate2 = SelectedDate.Substring(SelectedDate.Length - 4, 4);
            IWebElement Search = driver.FindElement(By.XPath("//span[text()='Search']"));
            Search.Click();

            driver.SwitchTo().Window(driver.WindowHandles[1]);
            IWebElement SearchResultsElement = waitLong.Until(x => x.FindElement(By.XPath("//address[contains (@class, 'booking-flow')]")));
            string SearchResults = SearchResultsElement.Text;
            Assert.AreEqual("Kiev - Zhulyany (IEV) Copenhagen (CPH)", SearchResults, "The search results should be equal to the expected search results");
            bool SearchResultsElementDisplayed = SearchResultsElement.Displayed;
            Assert.True(SearchResultsElementDisplayed, "SearchResults should be Displayed");

            IWebElement ResultDateElement = waitLong.Until(g => g.FindElement(By.XPath("//td[contains (@class, 'booking-flow')]/time")));
            bool ResultDateDisplayed = ResultDateElement.Displayed;
            Assert.True(ResultDateDisplayed, "Date should be displayed");
            string ResultDate = ResultDateElement.Text;
            StringAssert.Contains(SelectedDate1, ResultDate, "SelectedDate1 should contain ResultDate");
            StringAssert.Contains(SelectedDate2, ResultDate, "SelectedDate2 should contain ResultDate");

            IWebElement Cookie = driver.FindElement(By.XPath("//i[@class='cookie-policy__button__icon icon icon__x']"));
            Cookie.Click();
            IWebElement Basic = driver.FindElement(By.XPath("//p[contains (@class, 'booking-flow') and text() = 'Basic']"));
            bool BasicDisplayed = Basic.Displayed;
            Assert.True(BasicDisplayed, "Basic should be Displayed");

            IWebElement WizzGo = driver.FindElement(By.XPath("//p[contains (@class, 'booking-flow') and text() = 'WIZZ Go']"));
            bool WizzGoDisplayed = WizzGo.Displayed;
            Assert.True(WizzGoDisplayed, "WizzGo should be Displayed");

            IWebElement WizzPlus = driver.FindElement(By.XPath("//p[contains (@class, 'booking-flow') and text() = 'WIZZ Plus']"));
            bool WizzPlusDisplayed = WizzPlus.Displayed;
            Assert.True(WizzPlusDisplayed, "WizzPlus should be Displayed");

            IWebElement BasicPrice = driver.FindElement(By.XPath("//td[contains (@class, 'column--basic')]//strong"));
            bool BasicPriceDisplayed = BasicPrice.Displayed;
            Assert.True(BasicDisplayed, "BasicPrice should be Displayed");

            IWebElement WizzGoPrice = driver.FindElement(By.XPath("//td[contains (@class, 'column--middle')]//strong"));
            bool WizzGoPriceDisplayed = WizzGoPrice.Displayed;
            Assert.True(WizzGoDisplayed, "WizzGoPrice should be Displayed");

            IWebElement WizzPlusPrice = driver.FindElement(By.XPath("//td[contains (@class, 'column--plus')]//strong"));
            bool WizzPlusPriceDisplayed = WizzPlusPrice.Displayed;
            Assert.True(WizzPlusPriceDisplayed, "WizzPlusPrice should be Displayed");

            bool ReturnFlights = waitShort.Until(condition: ExpectedConditions.InvisibilityOfElementLocated(By.Id("fare-selector-return")));
            Assert.True(ReturnFlights, "ReturnFlights should be not displayed");
            IWebElement Showreturnflights = driver.FindElement(By.XPath("//button[text() = 'Show return flights']"));
            bool ShowreturnflightsDisplaed = Showreturnflights.Displayed;
            Assert.True(ShowreturnflightsDisplaed, "Showreturnflights should be displayed");

            IWebElement SelectedPrice = driver.FindElement(By.XPath("//td[contains (@class, 'column--middle')]//strong"));
            SelectedPrice.Click();

            IWebElement Continue = waitShort.Until(condition: ExpectedConditions.ElementToBeClickable(By.XPath("//div[@class = 'booking-flow__cta-container']/button")));
            Actions actionsContinue = new Actions(driver);
            actionsContinue.MoveToElement(Continue, 30, 10).Build().Perform();
            Continue.Click();

            IWebElement FirstName = waitLong.Until(n => n.FindElement(By.XPath("//input[@class = 'rf-input__input empty' and @placeholder = 'First name']")));
            FirstName.SendKeys("Ivan");
            IWebElement LastName = driver.FindElement(By.XPath("//input[@class = 'rf-input__input empty' and @placeholder = 'Last name']"));
            LastName.SendKeys("Ivanov");
            IWebElement Gender = waitShort.Until(condition: ExpectedConditions.ElementToBeClickable(By.XPath("//label[@for='passenger-gender-0-male']")));
            Actions actionsGender = new Actions(driver);
            actionsGender.MoveToElement(Gender, 10, 10).Build().Perform();
            Gender.Click();

            IWebElement RouteCorrectElement = driver.FindElement(By.XPath("//div[@class = 'booking-flow__itinerary__route']"));
            string RouteCorrect = RouteCorrectElement.Text;
            Assert.AreEqual("KIEV - ZHULYANY — COPENHAGEN", RouteCorrect, "The route should be equal to the expected route");
            bool RouteCorrectDisplayed = RouteCorrectElement.Displayed;
            Assert.True(RouteCorrectDisplayed, "Route should be displayed");
            IWebElement PassengersElement = driver.FindElement(By.XPath("//div[contains (@class, 'pax-number')]"));
            string Passengers = PassengersElement.Text;
            Assert.AreEqual("1 PASSENGER", Passengers, "Should be 1 PASSENGER");
            bool PassengersDisplayed = PassengersElement.Displayed;
            Assert.True(PassengersDisplayed, "Number of passengers should be displayed");

            IWebElement Continue2 = waitShort.Until(condition: ExpectedConditions.ElementToBeClickable(By.XPath("//div[@class = 'booking-flow__cta-container']/button")));
            Actions actionsContinue2 = new Actions(driver);
            actionsContinue2.MoveToElement(Continue2, 30, 10).Build().Perform();
            Continue2.Click();

            IWebElement Signin = waitShort.Until(s=> s.FindElement(By.Id("login-modal")));
            bool SigninDisplayed = Signin.Displayed;
            Assert.True(SigninDisplayed, "Signin should be displayed");      
        }
        [TearDown]
        public void Close()
        {
            driver.Quit();
        }
    }
}
