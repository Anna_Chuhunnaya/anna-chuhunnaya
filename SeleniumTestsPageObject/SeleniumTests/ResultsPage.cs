﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;



namespace SeleniumTests
{
    public class ResultsPage : BasePage
    {


        public ResultsPage(IWebDriver driver) : base(driver)
        {

        }

        [FindsBy(How = How.XPath, Using = "//address[contains (@class, 'booking-flow')]")]
        public IWebElement SearchResultsElement;

        [FindsBy(How = How.XPath, Using = "//td[contains (@class, 'booking-flow')]/time")]
        public IWebElement ResultDateElement;

        [FindsBy(How = How.XPath, Using = "//i[@class='cookie-policy__button__icon icon icon__x']")]
        public IWebElement Cookie;

        [FindsBy(How = How.XPath, Using = "//p[contains (@class, 'booking-flow') and text() = 'Basic']")]
        public IWebElement Basic;

        [FindsBy(How = How.XPath, Using = "//p[contains (@class, 'booking-flow') and text() = 'WIZZ Go']")]
        public IWebElement WizzGo;

        [FindsBy(How = How.XPath, Using = "//p[contains (@class, 'booking-flow') and text() = 'WIZZ Plus']")]
        public IWebElement WizzPlus;

        [FindsBy(How = How.XPath, Using = "//td[contains (@class, 'column--basic')]//strong")]
        public IWebElement BasicPrice;

        [FindsBy(How = How.XPath, Using = "//td[contains (@class, 'column--middle')]//strong")]
        public IWebElement WizzGoPrice;

        [FindsBy(How = How.XPath, Using = "//td[contains (@class, 'column--plus')]//strong")]
        public IWebElement WizzPlusPrice;


        [FindsBy(How = How.XPath, Using = "//button[text() = 'Show return flights']")]
        public IWebElement Showreturnflights;

        [FindsBy(How = How.XPath, Using = "//td[contains (@class, 'column--middle')]//p[@class = 'rf-fare__alternative__title']")]
        public IWebElement SelectedPrice;

        [FindsBy(How = How.XPath, Using = "//div[@class = 'booking-flow__cta-container']/button")]
        public IWebElement Continue;

        public bool ElementPresent()
        {
            try
            {
                driver.FindElement(By.Id("fare-selector-return"));
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
        }
    }
}
