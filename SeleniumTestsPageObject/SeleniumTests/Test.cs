﻿using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Interactions;
using OpenQA.Selenium.Support.UI;
using System;

namespace SeleniumTests
{
    [TestFixture]

    public class Test : BaseTest
    {

        [Test]
        public void Searchflights2()
        {
            WebDriverWait waitLong = new WebDriverWait(Driver, TimeSpan.FromSeconds(80));
            WebDriverWait waitShort = new WebDriverWait(Driver, TimeSpan.FromSeconds(20));
            InitPages();
            FollowLink("https://wizzair.com");
            waitLong.Until(dr => FlightSearchPage.DepartureStation.Displayed);
            FlightSearchPage.DepartureStation.Clear();
            FlightSearchPage.DepartureStation.SendKeys("Kiev");
            FlightSearchPage.DepartureStationKiev.Click();
            FlightSearchPage.ArrivalStation.SendKeys("Copenhagen");
            FlightSearchPage.ArrivalStationCopenhagen.Click();
            string SelectedDate = FlightSearchPage.SelectedDateElement.Text;
            DateTime SelectedDate1 = DateTime.Parse(SelectedDate);
            FlightSearchPage.Search.Click();

            Driver.SwitchTo().Window(Driver.WindowHandles[1]);
            waitLong.Until(dr => ResultsPage.SearchResultsElement.Displayed);
            string SearchResults = ResultsPage.SearchResultsElement.Text;
            Assert.AreEqual("Kiev - Zhulyany (IEV) Copenhagen (CPH)", SearchResults, "The search results should be equal to the expected search results");
            bool SearchResultsElementDisplayed = ResultsPage.SearchResultsElement.Displayed;
            Assert.True(SearchResultsElementDisplayed, "SearchResults should be Displayed");

            waitLong.Until(dr => ResultsPage.ResultDateElement.Displayed);
            bool ResultDateDisplayed = ResultsPage.ResultDateElement.Displayed;
            Assert.True(ResultDateDisplayed, "Date should be displayed");
            string ResultDate = ResultsPage.ResultDateElement.Text;
            DateTime ResultDate1 = DateTime.Parse(ResultDate);
            Assert.AreEqual(SelectedDate1, ResultDate1, "SelectedDate1 should contain ResultDate");

            ResultsPage.Cookie.Click();
            bool BasicDisplayed = ResultsPage.Basic.Displayed;
            Assert.True(BasicDisplayed, "Basic should be Displayed");
            bool WizzGoDisplayed = ResultsPage.WizzGo.Displayed;
            Assert.True(WizzGoDisplayed, "WizzGo should be Displayed");
            
            bool WizzPlusDisplayed = ResultsPage.WizzPlus.Displayed;
            Assert.True(WizzPlusDisplayed, "WizzPlus should be Displayed");
            bool BasicPriceDisplayed = ResultsPage.BasicPrice.Displayed;
            Assert.True(BasicDisplayed, "BasicPrice should be Displayed");
            bool WizzGoPriceDisplayed = ResultsPage.WizzGoPrice.Displayed;
            Assert.True(WizzGoDisplayed, "WizzGoPrice should be Displayed");
            bool WizzPlusPriceDisplayed = ResultsPage.WizzPlusPrice.Displayed;
            Assert.True(WizzPlusPriceDisplayed, "WizzPlusPrice should be Displayed");

            bool ReturnFlights = ResultsPage.ElementPresent();
            Assert.False(ReturnFlights, "ReturnFlights should be not displayed");
            bool ShowreturnflightsDisplaed = ResultsPage.Showreturnflights.Displayed;
            Assert.True(ShowreturnflightsDisplaed, "Showreturnflights should be displayed");
            ResultsPage.SelectedPrice.Click();
            waitShort.Until(dr => ResultsPage.Continue.Displayed);
            Actions actionsContinue = new Actions(Driver);
            actionsContinue.MoveToElement(ResultsPage.Continue, 10, 10).Build().Perform();
            ResultsPage.Continue.Click();

            waitLong.Until(dr => EnteringPersonalData.FirstName.Displayed);
            EnteringPersonalData.FirstName.SendKeys("Ivan");
            EnteringPersonalData.LastName.SendKeys("Ivanov");
            waitShort.Until(dr => EnteringPersonalData.Gender.Displayed);
            Actions actionsGender = new Actions(Driver);
            actionsGender.MoveToElement(EnteringPersonalData.Gender, 10, 10).Build().Perform();
            EnteringPersonalData.Gender.Click();

            string RouteCorrect = EnteringPersonalData.RouteCorrectElement.Text;
            Assert.AreEqual("KIEV - ZHULYANY — COPENHAGEN", RouteCorrect, "The route should be equal to the expected route");
            bool RouteCorrectDisplayed = EnteringPersonalData.RouteCorrectElement.Displayed;
            Assert.True(RouteCorrectDisplayed, "Route should be displayed");

            string Passengers = EnteringPersonalData.PassengersElement.Text;
            Assert.AreEqual("1 PASSENGER", Passengers, "Should be 1 PASSENGER");
            bool PassengersDisplayed = EnteringPersonalData.PassengersElement.Displayed;
            Assert.True(PassengersDisplayed, "Number of passengers should be displayed");

            waitShort.Until(dr => EnteringPersonalData.Continue2.Displayed);
            Actions actionsContinue2 = new Actions(Driver);
            actionsContinue2.MoveToElement(EnteringPersonalData.Continue2, 30, 10).Build().Perform();
            EnteringPersonalData.Continue2.Click();

            bool SigninDisplayed = EnteringPersonalData.Signin.Displayed;
            Assert.True(SigninDisplayed, "Signin should be displayed");
        }
        [TearDown]
        public void TearDown()
        {
            CloseBrowser();
        }

    }
    
}
