﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;



namespace SeleniumTests
{

    public class FlightSearchPage : BasePage
    {
        public FlightSearchPage(IWebDriver driver) : base(driver)
        {

        }

        [FindsBy(How = How.Id, Using = "search-departure-station")]
        public IWebElement DepartureStation;

        [FindsBy(How = How.XPath, Using = "//strong[@class='locations-container__location__name']")]
        public IWebElement DepartureStationKiev;

        [FindsBy(How = How.Id, Using = "search-arrival-station")]
        public IWebElement ArrivalStation;

        [FindsBy(How = How.XPath, Using = "//strong[@class='locations-container__location__name']")]
        public IWebElement ArrivalStationCopenhagen;

        [FindsBy(How = How.XPath, Using = "//span[text()='Search']")]
        public IWebElement Search;

        [FindsBy(How = How.Id, Using = "search-departure-date")]
        public IWebElement SelectedDateElement;
    }   


    
}
