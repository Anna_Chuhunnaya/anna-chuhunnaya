﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;



namespace SeleniumTests
{
    public abstract class BasePage
    {
        protected readonly IWebDriver driver;

        public BasePage(IWebDriver driver)
        {

            this.driver = driver;

            PageFactory.InitElements(driver, this);

        }

    }
}
