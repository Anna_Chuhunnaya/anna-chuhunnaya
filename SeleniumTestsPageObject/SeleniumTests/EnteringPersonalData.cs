﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.PageObjects;



namespace SeleniumTests
{
    public class EnteringPersonalData : BasePage
    {                                         
        public EnteringPersonalData(IWebDriver driver) : base(driver)
        {
        }

       [FindsBy(How = How.XPath, Using = "//input[@class = 'rf-input__input empty' and @placeholder = 'First name']")]
        public IWebElement FirstName;

        [FindsBy(How = How.XPath, Using = "//input[@class = 'rf-input__input empty' and @placeholder = 'Last name']")]
        public IWebElement LastName;

        [FindsBy(How = How.XPath, Using = "//label[@for='passenger-gender-0-male']")]
        public IWebElement Gender;

        [FindsBy(How = How.XPath, Using = "//div[@class = 'booking-flow__itinerary__route']")]
        public IWebElement RouteCorrectElement;

        [FindsBy(How = How.XPath, Using = "//div[contains (@class, 'pax-number')]")]
        public IWebElement PassengersElement;

        [FindsBy(How = How.XPath, Using = "//div[@class = 'booking-flow__cta-container']/button")]
        public IWebElement Continue2;

        [FindsBy(How = How.Id, Using = "login-modal")]
        public IWebElement Signin;

    }
}
