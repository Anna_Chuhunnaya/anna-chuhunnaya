﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;


namespace SeleniumTests
{
    public class BaseTest
    {
        public IWebDriver Driver = new ChromeDriver(@"D:\ChromeDriver");
        public FlightSearchPage FlightSearchPage;
        public ResultsPage ResultsPage;
        public EnteringPersonalData EnteringPersonalData;

        public void InitPages()
           {
               FlightSearchPage = new FlightSearchPage(Driver);
               ResultsPage = new ResultsPage(Driver);
               EnteringPersonalData = new EnteringPersonalData(Driver);
           }

        public void FollowLink(string url)
        {
            Driver.Manage().Window.Maximize();
            Driver.Navigate().GoToUrl(url);
        }

        public void NextWindow()
        {
            Driver.SwitchTo().Window(Driver.WindowHandles[1]);
        }

            public void CloseBrowser()
        {
            Driver.Quit();
        }
    }
}
