﻿using System;
using Calc;
using NUnit.Framework;

namespace Tests
{
    [SetUpFixture]
    class OneTimeSetUpOneTimeTearDown
    {
        public Scientific calc;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            calc = new Scientific();
        }
        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            calc = null;
        }
    }
}
