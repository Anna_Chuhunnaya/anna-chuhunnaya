﻿using System;
using Calc;
using NUnit.Framework;

namespace Tests 
{
    [TestFixture, Description("Testing the scientific calculator.")]
    class Tests : OneTimeSetUpOneTimeTearDown
    {
        [Test]
        [Retry(3)]
        public void Addtest()
        {
            double res = calc.Add(3, 5);
            Assert.That(res, Is.EqualTo(10), "3+5 should be equal 8");
        }
        [Ignore("Ignore a test")]
        [Test]
        public void Subtest()
        {
            double res = calc.Sub(3, 5);
            Assert.That(res, Is.EqualTo(-2), "3-5 should be equal -2!");
        }
        [Test]
        [Retry(5)]
        public void Multtest()
        {
            double res = calc.Mult(3, 5);
            Assert.That(res, Is.EqualTo(18), "3*5 should be equal 15!");
        }
        [Test]
        public void Multtest3()
        {
            double res = calc.Mult(0, 5);
            Assert.That(res, Is.Zero, "0*5 should be equal 0!");
        }
        [Test]
        public void Divtest()
        {
            double res = calc.Div(15, 5);
            Assert.That(res, Is.EqualTo(3), "15/5 should be equal 3!");
        }
        [Test]
        [Repeat(3)]
        public void Addtest2()
        {
            double res = calc.Add(4, 9);
            Assert.That(res, Is.Positive, "Result should be positive");
        }
        [Test]
        [Ignore("Ignore a test")]
        public void Subtest2()
        {
            double res = calc.Sub(2, 8);
            Assert.That(res, Is.Negative, "Result should be negative");
        }
        [Test]
        public void Multtest2()
        {
            double res = calc.Mult(3, -5);
            Assert.That(res, Is.Negative, "Result should be negative");
        }
        [Test]
        public void Divtest2()
        {
            double res = calc.Div(-10, -5);
            Assert.That(res, Is.Positive, "Result should be positive");
        }
        [Test, Description("Checking the exponentiation function.")]
        public void Powtest()
        {
            double res = calc.Pow(3, 5);
            Assert.That(res, Is.EqualTo(243), "3^5 should be equal 243!");
        }
        [Test, Description("Checking the exponentiation function.")]
        public void Powtest2()
        {
            double res = calc.Pow(8, 3);
            Assert.That(res, Is.GreaterThan(8), "Result should be more than 8!");
        }
        [Test, Description("Checking the Square root function.")]
        public void Sqtest()
        {
            double res = calc.Sq(25);
            Assert.That(res, Is.EqualTo(8), "Square root of 25 should be equal 5!");
        }
        [Test, Order(2), Description("Checking the Square root function.")]
        public void Sqtest2()
        {
            double res = calc.Sq(100);
            Assert.That(res, Is.LessThan(100), "Result should be less than 100!");
        }
        [Test]
        public void Pertest()
        {
            double res = calc.Per(50, 2);
            Assert.That(res, Is.EqualTo(1), "2% of 50 should be equal 1!");
        }
        [Test, Order(1)]
        public void Pertest2()
        {
            double res = calc.Per(200, 20);
            Assert.That(res, Is.LessThan(200), "Result should be less than 200!");
        }
        [Test, Order(3)]
        public void Abstest()
        {
            double res = calc.Abs(-5);
            Assert.That(res, Is.EqualTo(5), "Actual result should be Absolute value!");
        }
        [Test]
        public void Abstest2()
        {
            double res = calc.Abs(-10);
            Assert.That(res, Is.Positive, "Actual result should be Absolute value.");
        }
        [Test]
        public void Sintest()
        {
            double res = calc.Sin(90);
            Assert.That(res, Is.EqualTo(1), "Sin(90) should be equal 1!");
        }
        [Test]
        public void Costest()
        {
            double res = calc.Cos(90);
            Assert.That(res, Is.EqualTo(0), "Cos(90) should be equal 0!");
        }
        [Test]
        public void Costest2()
        {
            double res = calc.Cos(90);
            Assert.That(res, Is.Zero, "Cos(90) should be equal 0!");
        }
        [Test]
        public void Tantest()
        {
            double res = calc.Tan(135);
            Assert.That(res, Is.EqualTo(-1), "Tan(135) should be equal -1!");
        }
        [Test]
        [Ignore("Ignore a test")]
        public void Asintest()
        {
            double res = calc.Asin(1);
            Assert.That(res, Is.EqualTo(90), "Asin(1) should be equal 90!");
        }
        [Test]
        public void Acostest()
        {
            double res = calc.Acos(0);
            Assert.That(res, Is.EqualTo(90), "Acos(0) should be equal 90!");
        }
        [Test]
        public void Atantest()
        {
            double res = calc.Atan(1);
            Assert.That(res, Is.EqualTo(45), "Atan(1) should be equal 45!");
        }
        [Test]
        public void Logtest()
        {
            double res = calc.Log(25, 5);
            Assert.That(res, Is.EqualTo(2), "Log(25, 5) should be equal 2!");
        }
        [Test]
        public void Log10test()
        {
            double res = calc.Log10(100);
            Assert.That(res, Is.EqualTo(2), "Log10(100) should be equal 2!");
        }
        [Test(ExpectedResult = 12)]
        public double Sqtest3()
        {
            return Math.Sqrt(144);
        }
        [Test(ExpectedResult = 27)]
        public double Powtest3()
        {
            return Math.Pow(3, 3);
        }
        [Test(ExpectedResult = 11)]
        public double Divtest3()
        {
            return 165 / 15;
        }
        [TestCase(-11, -8)]
        [TestCase(-51, -5)]
        [TestCase(-5, -9)]
        public void Multtest(int a, int b)
        {
            Assert.That(a * b, Is.Positive);
        }
        [TestCase(192, -12)]
        [TestCase(-124, 31)]
        [TestCase(72, -8)]
        public void Divtest(int a, int b)
        {
            Assert.That(a / b, Is.LessThan(0));
        }
        [TestCase(18, 18)]
        [TestCase(35, 43)]
        [TestCase(0, 8)]
        public void Subtest(int a, int b)
        {
            Assert.That(a - b, Is.LessThanOrEqualTo(0));
        }
        [TestCase(0, 17, 17)]
        [TestCase(30, 70, 100)]
        [TestCase(6, 12, 18)]
        public void Addtest(int a, int b, int c)
        {
            Assert.That(a + b, Is.EqualTo(c));
        }
        [Test]
        public void Pass()
        {
            Assert.Pass("This test should be Passed");
        }
        [Test]
        public void Fail()
        {
            Assert.Fail("This test should be Failed");
        }
        [Test]
        public void Contains()
        {
            Assert.That("thisstring", Does.Contain("thisstring"), "The first string shoud contain the second string");
        }
        [Test]
        public void Start()
        {
            Assert.That("thisstring", Does.StartWith("this"), "The string should start with this");
        }
        [Test]
        public void End()
        {
            Assert.That("thisstringend", Does.EndWith("end"), "The string should be end with end");
        }
        [Test]
        public void Ignoringcase()
        {
            StringAssert.AreEqualIgnoringCase("ThisString", "thisstring", "The first string shoud contain the second string");
        }

    }

}