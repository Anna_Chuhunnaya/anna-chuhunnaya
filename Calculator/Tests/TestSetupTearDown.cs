﻿using System;
using Calc;
using NUnit.Framework;

namespace Tests
{
    [TestFixture]
    public class TestSetupTearDown
    {
        Scientific calc;
        [SetUp]
        public void Setup()
        {
           calc = new Scientific();
        }
        [Test]
        public void Powtest5()
        {
            double res = calc.Pow(2, 5);
            Assert.That(res, Is.EqualTo(32), "2^5 should be equal 32!");
        }
        [Test]
        public void Multtes4()
        {
            double res = calc.Mult(-8, -5);
            Assert.That(res, Is.GreaterThan(0), "Result should be more than 0!");
        }
        [Test]
        public void Pertest4()
        {
            double res = calc.Per(300, 20);
            Assert.That(res, Is.LessThan(61), "Result should be less than 61!");
        }
        [Test]
        public void Abstest3()
        {
            double res = calc.Abs(-80);
            Assert.That(res, Is.Positive, "Actual result should be Absolute value.");
        }
        [Test]
        public void Subtest()
        {
            var calc = new Scientific();
            double res = calc.Sub(8, 8);
            Assert.That(res, Is.Zero, "8-8 should be equal 0!");
        }
                [Test]
        public void Logtest2()
        {
            double res = calc.Log(36, 6);
            Assert.That(res, Is.EqualTo(2), "Log(36, 6) should be equal 2!");
        }
        [TearDown]
        public void PowtestTear()
        {
            calc = null;
        }

    }
}
