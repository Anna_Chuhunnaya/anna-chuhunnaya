﻿using System;

namespace Calc
{
    public class Scientific : Calculator
    {
        public double Pow(double a, double b)
        {
            double c = 1;
            for (int i = 0; i < b; i++)
            {
                c = Mult(c, a);
            }
            return c;
        }
        public double Sq(double a)
        {
            double c;
            c = Math.Sqrt(a);
            return c;
        }
        public double Per(double a, double b)
        {
            double c;
            c = Div((Mult(a, b)), 100);
            return c;
        }
        public double Abs(double a)
        {
            double c;
            c = Math.Abs(a);
            return c;
        }
        public double Sin(double a)
        {
            double c;
            c = Math.Sin(a * Math.PI / 180);
            return Math.Round(c, 10); ;
        }
        public double Cos(double a)
        {
            double c;
            c = Math.Cos(a * Math.PI / 180);
            return Math.Round(c, 10);
        }
        public double Tan(double a)
        {
            double c;
            c = Math.Tan(a * Math.PI / 180);
            return Math.Round(c, 10);
        }
        public double Asin(double a)
        {
            double c;
            c = Math.Asin(a);
            return c * 180 / Math.PI;
        }
        public double Acos(double a)
        {
            double c;
            c = Math.Acos(a);
            return c * 180 / Math.PI;
        }
        public double Atan(double a)
        {
            double c;
            c = Math.Atan(a);
            return c * 180 / Math.PI;
        }
        public double Log(double a, double b)
        {
            double c;
            c = Math.Log(a, b);
            return c;
        }
        public double Log10(double a)
        {
            double c;
            c = Math.Log10(a);
            return c;
        }
    }
}

