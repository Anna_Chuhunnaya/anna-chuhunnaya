﻿using System;

namespace Calc
{
    public class Calculator
    {
        public double Add(double a, double b)
        {
            double c;
            c = a + b;
            return c;
        }
        public double Sub(double a, double b)
        {
            double c;
            c = a - b;
            return c;
        }
        public double Mult(double a, double b)
        {
            double c;
            c = a * b;
            return c;
        }
        public double Div(double a, double b)
        {
            double c;
            c = a / b;
            return c;
        }
    }
}
